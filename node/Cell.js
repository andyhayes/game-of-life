var LivingCell = function() {
    var neighbours = [];
    var aliveNeighbours = 0;
    
    var getName = function() {
        return 'LivingCell';
    };
    
    var addNeighbour = function(neighbour) {
        neighbours.push(neighbour);
    };
    
    var alive = function() {
        aliveNeighbours++;
    };
    
    var live = function() {
        for (var i in neighbours) {
            var neighbour = neighbours[i];
            neighbour.alive();
        }
    };
    
    var evolve = function() {
        if (aliveNeighbours === 2 || aliveNeighbours === 3) {
            return new LivingCell();
        }
        return new DeadCell();
    };
    
    return {
        getName: getName,
        addNeighbour: addNeighbour,
        alive: alive,
        live: live,
        evolve: evolve
    };
};

var DeadCell = function() {
    var neighbours = [];
    var aliveNeighbours = 0;
    
    var getName = function() {
        return 'DeadCell';
    };
    
    var addNeighbour = function(neighbour) {
        neighbours.push(neighbour);
    };
    
    var alive = function() {
        aliveNeighbours++;
    };
    
    var evolve = function() {
        if (aliveNeighbours === 3) {
            return new LivingCell();
        }
        return new DeadCell();
    };
    
    var live = function() {
    };
    
    return {
        getName: getName,
        addNeighbour: addNeighbour,
        alive: alive,
        live: live,
        evolve: evolve
    };
};

exports.LivingCell = LivingCell;
exports.DeadCell = DeadCell;