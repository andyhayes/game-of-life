var assert = require("assert");
var LivingCell = require("../Cell").LivingCell;

describe('LivingCell', function(){
    
  describe('#livingCellName', function(){
    it('called "LivingCell', function(){
        var cell = new LivingCell();
        assert.equal(cell.getName(), 'LivingCell');
    });
  });
  
  describe('#livingCellNeighbours', function(){
    it('should tell nieghbours it is alive on live', function(){
      var cell = new LivingCell();
      var CellListener = function() {
          var state = '';
          var alive = function() {
              state = 'alive';
          };
          var dead = function() {
              state = 'dead';
          };
          var getState = function() {
              return state;
          };
          return {
              alive: alive,
              dead: dead,
              getState: getState
          };
      };
      var listener = new CellListener();
      var listener2 = new CellListener();
      cell.addNeighbour(listener);
      cell.addNeighbour(listener2);
      cell.live();
      assert.equal(listener.getState(), 'alive');
      assert.equal(listener2.getState(), 'alive');
    });
  });
  
  describe('#livingCellDies', function(){
    it('should become dead when no alive neighbours', function(){
      var cell = new LivingCell();
      var evolvedCell = cell.evolve();
      assert.equal(evolvedCell.getName(), 'DeadCell');
    });
    
    it('should become dead when 4 alive neighbours', function(){
      var cell = new LivingCell();
      cell.alive();
      cell.alive();
      cell.alive();
      cell.alive();
      var evolvedCell = cell.evolve();
      assert.equal(evolvedCell.getName(), 'DeadCell');
    });
  });
  
  describe('#livingCellStaysAlive', function(){
    it('should stay alive when 2 alive neighbours', function(){
      var cell = new LivingCell();
      cell.alive();
      cell.alive();
      var evolvedCell = cell.evolve();
      assert.equal(evolvedCell.getName(), 'LivingCell');
    });
    
    it('should stay alive when 3 alive neighbours', function(){
      var cell = new LivingCell();
      cell.alive();
      cell.alive();
      cell.alive();
      var evolvedCell = cell.evolve();
      assert.equal(evolvedCell.getName(), 'LivingCell');
    });
  });
});