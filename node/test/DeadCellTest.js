var assert = require("assert");
var DeadCell = require("../Cell").DeadCell;

describe('DeadCell', function(){
  
  describe('#deadCellName', function(){
    it('called "DeadCell', function(){
        var cell = new DeadCell();
        assert.equal(cell.getName(), 'DeadCell');
    });
  });
  
  describe('#deadCellNeighbours', function(){
    it('does nothing on live', function(){
      var cell = new DeadCell();
      var CellListener = function() {
          var state = '';
          var alive = function() {
              state = 'alive';
          };
          var dead = function() {
              state = 'dead';
          };
          var getState = function() {
              return state;
          };
          return {
              alive: alive,
              dead: dead,
              getState: getState
          };
      };
      var listener = new CellListener();
      cell.addNeighbour(listener);
      cell.live();
      assert.equal(listener.getState(), '');
    });
  });
  
  describe('#deadCellStaysDead', function(){
    it('should stay dead when no alive neighbours', function(){
      var cell = new DeadCell();
      var evolvedCell = cell.evolve();
      assert.equal(evolvedCell.getName(), 'DeadCell');
    });
  });
  
  describe('#deadCellBecomesAlive', function(){
    it('should become alive when 3 alive neighbours', function(){
      var cell = new DeadCell();
      cell.alive();
      cell.alive();
      cell.alive();
      var evolvedCell = cell.evolve();
      assert.equal(evolvedCell.getName(), 'LivingCell');
    });
  });
  
});