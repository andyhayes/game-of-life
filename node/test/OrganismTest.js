var assert = require("assert");
var Organism = require("../Organism").Organism;

describe('Organism', function(){
    
  describe('#displaysSingleCell', function(){
    it('displays a single alive cell as an "x"', function(){
        var organism = new Organism('x');
        assert.equal(organism.display(), 'x');
    });
    it('displays a single dead cell as a space', function(){
        var organism = new Organism(' ');
        assert.equal(organism.display(), ' ');
    });
  });
  
  describe('#singleCellEvolution', function(){
    it('evolves a single alive cell organism', function(){
        var organism = new Organism('x');
        organism.evolve();
        assert.equal(organism.display(), ' ');
    });
  });
  
  describe('#simpleEvolution', function(){
    it('evolves three dead cells', function(){
        var organism = new Organism('   ');
        organism.evolve();
        assert.equal(organism.display(), '   ');
    });
    it('evolves three living cells into one living cell', function(){
        var organism = new Organism('xxx');
        organism.evolve();
        assert.equal(organism.display(), ' x ');
    });
    it('evolves four living cells into two living cells', function(){
        var organism = new Organism('xxxx');
        organism.evolve();
        assert.equal(organism.display(), ' xx ');
    });
  });
  
  describe('#2dEvolution', function(){
    it('evolves 2x3 living cells correctly', function(){
        var organism = new Organism(' x \n' +
                                    'xxx');
        organism.evolve();
        assert.equal(organism.display(), 'xxx\n' +
                                         'xxx');
    });
  });
  
  describe('#2dEvolutionSecondGeneration', function(){
    it('2x2 block stays alive after two generations', function(){
        var organism = new Organism('xx\n' +
                                    'xx');
        organism.evolve();
        organism.evolve();
        assert.equal(organism.display(), 'xx\n' +
                                         'xx');
    });
    it('2x3 block evolves correctly after two generations', function(){
        var organism = new Organism(' x \n' +
                                    'xxx');
        organism.evolve();
        organism.evolve();
        assert.equal(organism.display(), 'x x\n' +
                                         'x x');
    });
  });
});