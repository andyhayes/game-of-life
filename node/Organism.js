var Cell = require('./Cell');
var LivingCell = Cell.LivingCell;
var DeadCell = Cell.DeadCell;

var Organism = function(initialState) {
    
    var cells = [];
    
    var initialiseState = function() {
        var initialcellRows = initialState.split('\n');
        for (var i in initialcellRows) {
            var lineCells = [];
            var initialCellStates = initialcellRows[i].split('');
            for (var ic in initialCellStates) {
                if (initialCellStates[ic] === 'x') {
                    lineCells.push(new LivingCell());
                } else {
                    lineCells.push(new DeadCell());
                }
            }
            cells.push(lineCells);
        }
    };
    
    var setUpNeighbours = function() {
        for (var c = 0; c < cells.length; c++) {
            var cellRow = cells[c];
            for (var cellRowIndex = 0; cellRowIndex < cellRow.length; cellRowIndex++) {
                var cell = cellRow[cellRowIndex];
                // left neighbour
                if (cellRowIndex > 0) {
                    cell.addNeighbour(cellRow[cellRowIndex - 1]);
                }
                // right neighbour
                if (cellRowIndex < (cellRow.length - 1)) {
                    cell.addNeighbour(cellRow[cellRowIndex + 1]);
                }
                // above-left neighbour
                if (c > 0 && cellRowIndex > 0) {
                    cell.addNeighbour(cells[c - 1][cellRowIndex - 1]);
                }
                // above neighbour
                if (c > 0) {
                    cell.addNeighbour(cells[c - 1][cellRowIndex]);
                }
                // above-right neighbour
                if (c > 0 && cellRowIndex < (cellRow.length - 1)) {
                    cell.addNeighbour(cells[c - 1][cellRowIndex + 1]);
                }
                // below-left neighbour
                if (c < (cells.length - 1) &&  cellRowIndex > 0) {
                    cell.addNeighbour(cells[c + 1][cellRowIndex - 1]);
                }
                // below neighbour
                if (c < (cells.length - 1)) {
                    cell.addNeighbour(cells[c + 1][cellRowIndex]);
                }
                // below-right neighbour
                if (c < (cells.length - 1) &&  cellRowIndex < (cellRow.length - 1)) {
                    cell.addNeighbour(cells[c + 1][cellRowIndex + 1]);
                }
            }
        }
    };
    
    var display = function() {
        var states = '';
        for (var c in cells) {
            if (c > 0) {
                states += '\n';
            }
            var cellRow = cells[c];
            for (var cellRowIndex = 0; cellRowIndex < cellRow.length; cellRowIndex++) {
                var cell = cellRow[cellRowIndex];
                if (cell.getName() === 'LivingCell') {
                    states += 'x';
                } else {
                    states += ' ';
                }
            }
        }
        return states;
    };
    
    var liveCells = function() {
        for (var c in cells) {
            var cellRow = cells[c];
            for (var cellRowIndex = 0; cellRowIndex < cellRow.length; cellRowIndex++) {
                var cell = cellRow[cellRowIndex];
                cell.live();
            }
        }
    };
    
    var evolveCells = function() {
        for (var c in cells) {
            var cellRow = cells[c];
            for (var cellRowIndex = 0; cellRowIndex < cellRow.length; cellRowIndex++) {
                var cell = cellRow[cellRowIndex];
                cellRow[cellRowIndex] = cell.evolve();
            }
        }
    };
    
    var evolve = function() {
        setUpNeighbours();
        liveCells();
        evolveCells();
    };
    
    initialiseState();
    
    return {
        display: display,
        evolve: evolve
    };
};

exports.Organism = Organism;