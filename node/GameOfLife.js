var fs = require('fs');
var Organism = require('./Organism').Organism;

function display(organism) {
    console.log('--------------------');
    console.log(organism.display());
}

fs.readFile('node/gol-gun.txt', 'utf8', function (err, data) {
    if (err) throw err;
    
    var organism = new Organism(data);
    display(organism);
    setInterval(function() {
        organism.evolve();
        display(organism);
    }, 200);
});